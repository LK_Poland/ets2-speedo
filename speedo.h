#ifndef SPEEDO_H
#define SPEEDO_H

#include <QElapsedTimer>
#include <QImage>
#include <QMenu>
#include <QtSerialPort/QSerialPort>
#include <QSettings>
#include <QSystemTrayIcon>
#include <QtNetwork/QNetworkAccessManager>
#include <QWidget>

class Speedo : public QWidget
{
        Q_OBJECT

    public:
        Speedo(QWidget *parent = nullptr);
        QSize sizeHint() const override;

    protected:
        void mouseMoveEvent(QMouseEvent *event) override;
        void mousePressEvent(QMouseEvent *event) override;
        void paintEvent(QPaintEvent *event) override;
        void resizeEvent(QResizeEvent *event) override;
        void closeEvent(QCloseEvent *event) override;
        void contextMenuEvent(QContextMenuEvent *event) override;

    private slots:
        void showAbout();
        void updateTelemetry();

    private:
        QSystemTrayIcon trayIcon;
        QMenu trayMenu;
        QImage imgFuel;
        QNetworkAccessManager nam;
        QPoint dragPosition;
        QSettings settings;
        QSerialPort serial;
        QElapsedTimer elapsed;
        QString arduinoPortName;
        bool arduinoEnabled;
        double speed = 0;
        int gear = 0;
        double speedLimit = 0;
        double fuel = 0;
        double fuelCapacity = 0;
        bool blinkerLeftActive = false;
        bool blinkerRightActive = false;
        bool blinkerLeftOn = false;
        bool blinkerRightOn = false;
        bool blinkersEmergency = false;
        double GetX(double angle, double radius);
        double GetY(double angle, double radius);
};

#endif
