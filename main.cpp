#include <QApplication>

#include "speedo.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    Speedo speedo;
    speedo.show();
    return app.exec();
}
