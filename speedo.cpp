#include "speedo.h"

#include <QAction>
#include <QApplication>
#include <QCloseEvent>
#include <QCoreApplication>
#include <QDesktopWidget>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QMouseEvent>
#include <QNetworkReply>
#include <QPainter>
#include <QTimer>

#include <cmath>

static const QString& AppName = "ETS2 Speedo";
static const QString& AppVersion = "1.0";
static const QString& AppAuthor = "LK";
static const QString& AppEMail = "lukasz86@10g.pl";

Speedo::Speedo(QWidget *parent) :
    QWidget(parent, Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint),
    imgFuel("img/fuel24.png"),
    settings("speedo.ini", QSettings::IniFormat)
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, QOverload<>::of(&Speedo::update));
    timer->start(50);

    QAction *aboutAction = new QAction(tr("About..."), this);
    connect(aboutAction, SIGNAL(triggered()), this, SLOT(showAbout()));
    addAction(aboutAction);

    QAction *quitAction = new QAction(tr("E&xit"), this);
    quitAction->setShortcut(tr("Ctrl+Q"));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));
    addAction(quitAction);

    trayMenu.addAction(aboutAction);
    trayMenu.addSeparator();
    trayMenu.addAction(quitAction);

    trayIcon.setIcon(QIcon("img/speedo.png"));
    trayIcon.setToolTip(QString("%1 %2 by %3").arg(AppName).arg(AppVersion).arg(AppAuthor));
    trayIcon.setContextMenu(&trayMenu);
    trayIcon.show();

    setToolTip(tr("Drag speedo with the left mouse button.\n"
                  "Use right mouse button to open context menu."));
    setWindowTitle(AppName);
    const QSize &size = sizeHint();
    setGeometry(settings.value("position/x", (QApplication::desktop()->width() - size.width()) / 2).toInt(),
                settings.value("position/y", (QApplication::desktop()->height() - size.height()) / 2).toInt(),
                size.width(), size.height());

    arduinoEnabled = settings.value("arduino/enabled", "false").toBool();
    arduinoPortName = settings.value("arduino/port", "").toString();
    if (arduinoEnabled && arduinoPortName.length())
    {
        serial.setPortName(arduinoPortName);
        serial.setParity(QSerialPort::NoParity);
        serial.setBaudRate(2000000);
        serial.setDataBits(QSerialPort::Data8);
        serial.setStopBits(QSerialPort::OneStop);
        serial.setFlowControl(QSerialPort::NoFlowControl);
        if (!serial.open(QSerialPort::WriteOnly))
        {
            qDebug() << "Failed to open " << arduinoPortName;
        }
        else
        {
            elapsed.start();
        }
    }
}

void Speedo::mousePressEvent(QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        dragPosition = event->globalPos() - frameGeometry().topLeft();
        event->accept();
    }
}

void Speedo::mouseMoveEvent(QMouseEvent *event)
{
    if (event->buttons() & Qt::LeftButton)
    {
        move(event->globalPos() - dragPosition);
        event->accept();
    }
}

void Speedo::paintEvent(QPaintEvent *)
{
    QNetworkReply *reply =
            nam.get(QNetworkRequest(QUrl("http://127.0.0.1:25555/api/ets2/telemetry")));
    connect(reply, SIGNAL(finished()), this, SLOT(updateTelemetry()));

    static const QPoint hand[3] =
    {
        QPoint(7, 8),
        QPoint(-7, 8),
        QPoint(0, -70)
    };
    const double radius = width() / 2.5;
    const double angleStep = 270. / 15.;
    const double textOffsetX = 10.;
    const int arcRadius = 60;
    const QString &strGear = gear == -1 ? "R" : (gear == 0 ? "N" : QString::number(gear));
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.fillRect(rect(), Qt::black);
    painter.translate(width() / 2, height() / 2);
    painter.setPen(Qt::white);
    painter.drawText(-15, -25, "km/h");
    painter.drawText(QRect(-10, +25, 20, 10), Qt::AlignCenter, strGear);
    painter.save();
    painter.rotate(45.);
    painter.drawArc(-arcRadius, -arcRadius, 2 * arcRadius, 2 * arcRadius, 16 * 0, 16 * 270);
    painter.restore();
    painter.save();
    painter.rotate(speed / 10. * angleStep - 135.);
    if (speed - 3. > speedLimit)
    {
        painter.setBrush(Qt::red);
    }
    else
    {
        painter.setBrush(Qt::white);
    }
    painter.drawConvexPolygon(hand, 3);
    painter.restore();
    painter.setBrush(Qt::white);
    for (int i = 0; i < 16; i++)
    {
        const double xpos = GetX(i * angleStep + 135, radius) - textOffsetX;
        const double ypos = GetY(i * angleStep + 135, radius);
        const double spd = i * 10;
        if (spd > speedLimit)
        {
            painter.setPen(Qt::gray);
        }
        else
        {
            painter.setPen(Qt::white);
        }
        painter.save();
        painter.rotate(i * angleStep + 135.);
        painter.drawLine(radius - 20., 0, radius - 15., 0);
        painter.restore();
        if (i < 15)
        {
            painter.save();
            painter.rotate((i + 0.5) * angleStep + 135.);
            painter.drawLine(radius - 20., 0, radius - 17., 0);
            painter.restore();
        }
        painter.drawText(xpos, ypos, QString::number(spd));
    }
    double fuelPercent = fuel * 100. / fuelCapacity;
    if (fuelPercent < 50.)
    {
        fuelPercent = std::max(0., fuelPercent - 15.);
    }
    const int fuelEaten = (100 - fuelPercent) / 100 * imgFuel.height();
    QImage image(imgFuel);
    for (int row = 0 ; row < fuelEaten; ++row)
    {
        for (int col = 0 ; col < image.width() ; ++col)
        {
            const QColor &color = image.pixelColor(col, row);
            image.setPixelColor(col, row, color.darker());
        }
    }
    painter.drawImage(-image.width() / 2, 2 * image.height(), image);
}

double Speedo::GetX(double angle, double radius)
{
    return radius * cos(angle / 360. * 2. * M_PI);
}

double Speedo::GetY(double angle, double radius)
{
    return radius * sin(angle / 360. * 2. * M_PI);
}

void Speedo::resizeEvent(QResizeEvent *)
{
    const int side = qMin(width(), height());
    QRegion maskedRegion(width() / 2 - side / 2, height() / 2 - side / 2, side,
                         side, QRegion::Ellipse);
    setMask(maskedRegion);
}

void Speedo::closeEvent(QCloseEvent *event)
{
    settings.setValue("position/x", x());
    settings.setValue("position/y", y());
    settings.setValue("arduino/enabled", arduinoEnabled);
    settings.setValue("arduino/port", arduinoPortName);
    QWidget::closeEvent(event);
}

void Speedo::contextMenuEvent(QContextMenuEvent *event)
{
    trayMenu.exec(event->globalPos());
}

void Speedo::showAbout()
{
    QMessageBox::information(this, "About", QString("%1 ver. %2\nwritten by %3\nemail: %4").arg(AppName).arg(AppVersion).arg(AppAuthor).arg(AppEMail));
}

void Speedo::updateTelemetry()
{
    QNetworkReply *reply = (QNetworkReply*)sender();
    if (reply->error() == QNetworkReply::NoError)
    {
        const QString &TRUCK = "truck";
        const QString &SPEED = "speed";
        const QString &GEAR = "gear";
        const QString &FUEL = "fuel";
        const QString &FUEL_CAPACITY = "fuelCapacity";
        const QString &NAVI = "navigation";
        const QString &SPEED_LIMIT = "speedLimit";
        const QString &BLINKER_LEFT_ACTIVE = "blinkerLeftActive";
        const QString &BLINKER_RIGHT_ACTIVE = "blinkerRightActive";
        const QString &BLINKER_LEFT_ON = "blinkerLeftOn";
        const QString &BLINKER_RIGHT_ON = "blinkerRightOn";
        const QString &text = reply->readAll();
        const QJsonDocument &doc = QJsonDocument::fromJson(text.toUtf8());
        const QJsonObject &obj = doc.object();
        const QVariantMap &map = obj.toVariantMap();
        if (map.contains(TRUCK))
        {
            const QVariantMap &truck = map[TRUCK].toMap();
            if (truck.contains(SPEED))
            {
                speed = fabs(truck[SPEED].toDouble());
            }
            if (truck.contains(GEAR))
            {
                gear = truck[GEAR].toInt();
            }
            if (truck.contains(FUEL))
            {
                fuel = truck[FUEL].toDouble();
            }
            if (truck.contains(FUEL_CAPACITY))
            {
                fuelCapacity = truck[FUEL_CAPACITY].toDouble();
            }
            if (truck.contains(BLINKER_LEFT_ACTIVE))
            {
                blinkerLeftActive = truck[BLINKER_LEFT_ACTIVE].toBool();
            }
            if (truck.contains(BLINKER_RIGHT_ACTIVE))
            {
                blinkerRightActive = truck[BLINKER_RIGHT_ACTIVE].toBool();
            }
            if (truck.contains(BLINKER_LEFT_ON))
            {
                blinkerLeftOn = truck[BLINKER_LEFT_ON].toBool();
            }
            if (truck.contains(BLINKER_RIGHT_ON))
            {
                blinkerRightOn = truck[BLINKER_RIGHT_ON].toBool();
            }
        }
        if (map.contains(NAVI))
        {
            const QVariantMap &navi = map[NAVI].toMap();
            if (navi.contains(SPEED_LIMIT))
            {
                speedLimit = navi[SPEED_LIMIT].toDouble();
                if (0. == speedLimit)
                {
                    speedLimit = 999.;
                }
            }
        }
        if (serial.isOpen() &&
            elapsed.elapsed() > 250)
        {
            if (blinkersEmergency)
            {
                static int ctr = 0;
                if (!blinkerLeftOn && !blinkerRightOn)
                {
                    if (++ctr == 3)
                    {
                        blinkersEmergency = false;
                        ctr = 0;
                    }
                }
                else if (blinkerLeftOn == blinkerRightOn)
                {
                    ctr = 0;
                }
                else if (blinkerLeftOn != blinkerRightOn)
                {
                    blinkersEmergency = false;
                    ctr = 0;
                }
            }
            else
            {
                blinkersEmergency = blinkerLeftOn && blinkerRightOn;
            }
            serial.write(QString("speed=%1\n").arg(speed, 0, 'f', 1).toUtf8());
            serial.write(QString("speedLimit=%1\n").arg(speedLimit, 0, 'f', 1).toUtf8());
            serial.write(QString("fuel=%1\n").arg(fuel * 100. / fuelCapacity, 0, 'f', 1).toUtf8());
            serial.write(QString("blinkerL=%1\n").arg(blinkerLeftActive || blinkersEmergency).toUtf8());
            serial.write(QString("blinkerR=%1\n").arg(blinkerRightActive || blinkersEmergency).toUtf8());
            serial.flush();
            elapsed.restart();
        }
    }
    else
    {
        qDebug() << reply->errorString();
    }
    reply->deleteLater();
}

QSize Speedo::sizeHint() const
{
    return QSize(200, 200);
}
