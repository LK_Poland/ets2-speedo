#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
#define LEFT_BLINKER 2
#define RIGHT_BLINKER 3

Adafruit_SSD1306 display(OLED_RESET);

int speed = 0;
int speedLimit = 0;
int fuel = 0;
bool blinkerActiveL = false;
bool blinkerActiveR = false;
bool blinkerOn = false;
unsigned long long blinkerTime = 0;

void setup()
{
  pinMode(LEFT_BLINKER, OUTPUT);
  pinMode(RIGHT_BLINKER, OUTPUT);

  Serial.begin(2000000);
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.display();
}

void serialEvent()
{
  const String &text = Serial.readStringUntil('\n');
  if (text.indexOf("speed=") != -1)
  {
    //display.print("speed");
    speed = text.substring(text.indexOf("=") + 1).toFloat() + 0.5f;
  }
  else if (text.indexOf("speedLimit=") != -1)
  {
    //display.print("speedLimit");
    speedLimit = text.substring(text.indexOf("=") + 1).toInt();
  }
  else if (text.indexOf("fuel=") != -1)
  {
    //display.print("fuel");
    fuel = text.substring(text.indexOf("=") + 1).toFloat() + 0.5f;
  }
  else if (text.indexOf("blinkerL=") != -1)
  {
    //display.print("blinkerL");
    blinkerActiveL = text.substring(text.indexOf("=") + 1) == "1";
  }
  else if (text.indexOf("blinkerR=") != -1)
  {
    //display.print("blinkerR");
    blinkerActiveR = text.substring(text.indexOf("=") + 1) == "1";
  }
}

void animateBlinker(const unsigned long long timeStamp,
                    const int pin, const bool doUpdate)
{
  if (timeStamp - blinkerTime > 499)
  {
    digitalWrite(pin, blinkerOn ? HIGH : LOW);
    if (doUpdate)
    {
      blinkerOn = !blinkerOn;
      blinkerTime = timeStamp;
    }
  }
}

void loop()
{
  const unsigned long long timeStamp = millis();
  if (blinkerActiveL && blinkerActiveR)
  {
    animateBlinker(timeStamp, LEFT_BLINKER, false);
    animateBlinker(timeStamp, RIGHT_BLINKER, true);
  }
  else if (blinkerActiveL)
  {
    digitalWrite(RIGHT_BLINKER, LOW);
    animateBlinker(timeStamp, LEFT_BLINKER, true);
  }
  else if (blinkerActiveR)
  {
    digitalWrite(LEFT_BLINKER, LOW);
    animateBlinker(timeStamp, RIGHT_BLINKER, true);
  }
  else
  {
    blinkerOn = false;
    digitalWrite(LEFT_BLINKER, LOW);
    digitalWrite(RIGHT_BLINKER, LOW);
  }
  display.setCursor(0, 0);
  display.clearDisplay();
  display.print("  ");
  display.print(speed);
  display.println(" km/h");
  if (speed > speedLimit)
  {
    display.println("  SLOWER!");
  }
  else
  {
    display.println();
  }
  display.println();
  display.print(" fuel:");
  if (fuel < 100)
  {
    display.print(" ");
  }
  display.print(fuel);
  display.println("%");
  display.display();
}
